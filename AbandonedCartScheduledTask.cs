﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

using CMS;
using CMS.Ecommerce;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.Helpers;
using CMS.ExtendedControls.ActionsConfig;
using CMS.Membership;
using CMS.Base;
using CMS.MacroEngine;

using CMS.Scheduler;
using CMS.EventLog;
using CMS.EmailEngine;
using CMS.OnlineMarketing;
using CMS.DataEngine;
using System.Data.Common;

[assembly: RegisterCustomClass("AbandonedCart.AbandonedCartTask", typeof(AbandonedCart.AbandonedCartTask))]
//[assembly: RegisterCustomClass("AbandonedCart.AbandonedCartTask", typeof(AbandonedCart.AbandonedCartTask))]

namespace AbandonedCart
{

    public class AbandonedCartActivity
    {
        public int ActivityID { get; set; }
        public bool ActivityIgnored { get; set; }

        public AbandonedCartActivity(ActivityInfo activity, bool ignored)
        {
            ActivityID=activity.ActivityID;
            ActivityIgnored=ignored;
        }
        public void Ignore()
        {
            ActivityIgnored=true;
        }
    }

    public class AbandonedCartProductCycle
    {
        public int SKUID { get; set; }
        public int SKUUnits { get; set; }
        public List<AbandonedCartActivity> Activities { get; set; }
        
   

        public AbandonedCartProductCycle(ActivityInfo activity)
        {
            Activities = new List<AbandonedCartActivity>();
            if (activity.ActivityType == "productaddedtoshoppingcart" || activity.ActivityType == "productunitschanged")
            {
                SKUID = activity.ActivityItemID;
                SKUUnits = Convert.ToInt32(activity.ActivityValue);
                Activities.Add(new AbandonedCartActivity(activity,false));
            }

        }

        public void ProcessActivity(ActivityInfo activity)
        {
            
            if (activity.ActivityType == "productaddedtoshoppingcart")
            {
                AddSKUUnits(activity);
            }
            if (activity.ActivityType == "productunitschanged")
            {
                ChangeSKUUnits(activity);
            }
            if (activity.ActivityType == "productremovedfromshoppingcart")
            {
                EndCycle(activity);
            }
         /*   if (activity.ActivityType == "purchase")
            {
 
            }                   */ // THIS NEED TO BE HANDLED BY THE CART OBJECT
        }

        public void AddSKUUnits(ActivityInfo activity)
        {
            if (activity.ActivityType == "productaddedtoshoppingcart")
            {
                if (activity.ActivityItemID==SKUID)
                {
                    SKUUnits += Convert.ToInt32(activity.ActivityValue);
                    Activities.Add(new AbandonedCartActivity(activity,false));
                }
            }
        }

        public void ChangeSKUUnits(ActivityInfo activity)
        {
            if (activity.ActivityType == "productunitschanged")
            {
                if (activity.ActivityItemID == SKUID)
                {
                    SKUUnits = Convert.ToInt32(activity.ActivityValue);
                    Activities.Add(new AbandonedCartActivity(activity,false));
                }
            }
        }

        public void EndCycle(ActivityInfo activity)
        {
            if (activity.ActivityType == "productremovedfromshoppingcart")
            {
                if (activity.ActivityItemID == SKUID)
                {
                    Activities.Add(new AbandonedCartActivity(activity,false));
                    SKUUnits = 0;
                    foreach(AbandonedCartActivity ac in Activities)
                    {
                        ac.Ignore();
                        AbandonedCart.AbandonedCartActivityInfoProvider.SetAbandonedCartActivityInfo(new AbandonedCart.AbandonedCartActivityInfo()
                        {
                            ActivityID=ac.ActivityID,
                            ActivityWasProcessed=ac.ActivityIgnored
                        }
                        );
                        
                    }
               //     Activities.Clear();
                }
            }
        }

        public void EndCycle_Purchase(ActivityInfo activity)
        {
            if (activity.ActivityType == "purchase")
            {
               
                    Activities.Add(new AbandonedCartActivity(activity, false));
                    SKUUnits = 0;
                    
                    foreach (AbandonedCartActivity ac in Activities)
                    {
                        ac.Ignore();
                        AbandonedCart.AbandonedCartActivityInfoProvider.SetAbandonedCartActivityInfo(new AbandonedCart.AbandonedCartActivityInfo()
                        {
                            ActivityID = ac.ActivityID,
                            ActivityWasProcessed = ac.ActivityIgnored
                        }
                        );
                        
                    }
           //         Activities.Clear();
                
            }
        }
    }

    public class AbandonedCartContactCart
    {
        public int ContactID { get; set; }
        public List<AbandonedCartProductCycle> ProductCycles { get; set; }
        public DateTime LastUpdate { get; set; }
        public DateTime LastEmailSent { get; set; }
        public int AbandonedCartContactCartID { get; set; }
        public AbandonedCartContactCartInfo CartInfo { get; set; }
        public string CurrentCouponCode { get; set; }

        public AbandonedCartContactCart(AbandonedCart.AbandonedCartContactCartInfo contactCart)
        {
            ContactID = contactCart.ContactID;
            LastUpdate = contactCart.LastUpdate;
            AbandonedCartContactCartID = contactCart.AbandonedCartContactCartID;
            LastEmailSent = contactCart.LastEmailSent;
            ProductCycles = new List<AbandonedCartProductCycle>();
            CartInfo = contactCart;
            CurrentCouponCode = contactCart.CurrentCouponCode;
        }

        public AbandonedCartContactCart()
        {
            ProductCycles = new List<AbandonedCartProductCycle>();
        }

        public void ProcessActivity(ActivityInfo activity)
        {
            if (activity.ActivityType == "purchase" || activity.ActivityType == "productaddedtoshoppingcart" || activity.ActivityType == "productunitschanged" || activity.ActivityType == "productremovedfromshoppingcart")
            {
                if (activity.ActivityType != "purchase")
                {


                    var cycle = ProductCycles.Find(x => x.SKUID == activity.ActivityItemID);
                    if (activity.ActivityType == "productaddedtoshoppingcart" || activity.ActivityType == "productunitschanged")
                    {
                        if (cycle == null)
                        {
                            ProductCycles.Add(new AbandonedCartProductCycle(activity));
                        }

                    }
                    if (cycle != null)
                    {
                        cycle.ProcessActivity(activity);
                    }

                }
                if (activity.ActivityType == "purchase")
                {
                    foreach (AbandonedCartProductCycle cy in ProductCycles)
                    {
                        cy.EndCycle_Purchase(activity);
                    }
                    this.CurrentCouponCode = "";
                }

                UpdateCart(activity);
            }
        }
        public void UpdateCart(ActivityInfo activity)
        {
            LastUpdate = activity.ActivityCreated;
        }

        public void SaveCart()
        {
            if (this.CartInfo == null)
            {
           //     EventLogProvider.LogEvent("I", "AbandonedCart", "Cart Object Saved - Overwrite", this.CartInfo.AbandonedCartContactCartID + " " + this.ContactID);
                AbandonedCart.AbandonedCartContactCartInfoProvider.SetAbandonedCartContactCartInfo(new AbandonedCart.AbandonedCartContactCartInfo()
                    {
                        LastUpdate = LastUpdate,
                        LastEmailSent = LastEmailSent,
                        ContactID = ContactID,
                        CurrentCouponCode = CurrentCouponCode,
                        SiteID = this.CartInfo.SiteID
                    }
                    );
            }
            else
            {
         //       EventLogProvider.LogEvent("I", "AbandonedCart", "Cart Object Saved - New", this.CartInfo.AbandonedCartContactCartID + " " + this.ContactID);
                CartInfo.LastUpdate = this.LastUpdate;
                CartInfo.LastEmailSent = this.LastEmailSent;
                CartInfo.ContactID = this.ContactID;
                CartInfo.CurrentCouponCode = this.CurrentCouponCode;
                AbandonedCart.AbandonedCartContactCartInfoProvider.SetAbandonedCartContactCartInfo(CartInfo);
            }
        }
    }

    

    public class AbandonedCartTask : ITask
    {
        public int timeSinceLastActivity { get; set; }
        public int timeSinceLastActivityUnit { get; set; }
        public int activitiesNotOlderThan { get; set; }
        public int activitiesNotOlderThanUnit { get; set; }
        public EmailPriorityEnum emailPriority  { get; set; }
        public string pageUrl { get; set; }
        public bool discountEnabled { get; set; }
        public CMS.DataEngine.ObjectQuery<CMS.OnlineMarketing.ActivityInfo> query { get; set; }
        public List<ContactInfo> contacts { get; set; }
        public List<ActivityInfo> activities { get; set; }
        public List<AbandonedCartContactCart> contactCarts { get; set; }
        public DiscountInfo discount { get; set; }
        public string couponPrefix { get; set; }
        public string discountName { get; set; }
        public bool allowUseParentSKUImage { get; set; }
        public string emailTemplateCodeName { get; set; }
        public EmailTemplateInfo emailTemplate { get; set; }
        public DateTime InstallDate { get; set; }
        public bool InstallDateSetting { get; set; }
        public bool  EmailTemplateIsGlobal { get; set; }
        public int SiteId { get; set; }
        public string  SiteCodeName { get; set; }

        public string Execute(TaskInfo ti)
        {
            var currentTime = DateTime.Now;

            Initialize(ti);
            LoadSettings();
            PrepareActivitiesQuery();
            GetActivities();
            GetOldCarts();
            HandleActivities();
            SendEmails();
            SaveChanges();

            EventLogProvider.LogEvent("I", "AbandonedCartTask", "Execution Time: " + (DateTime.Now - currentTime).ToString());

            return "";
        }

        public void Initialize(TaskInfo ti)
        {
            SiteId = ti.TaskSiteID;
            var InstallDateInfos = AbandonedCart.AbandonedCartSetupInfoProvider.GetAbandonedCartSetups().WhereEquals("SiteID",SiteId).ToList();
            if (InstallDateInfos == null || InstallDateInfos.Count <= 0)
            {
                var now = DateTime.Now;
                AbandonedCart.AbandonedCartSetupInfoProvider.SetAbandonedCartSetupInfo(new AbandonedCart.AbandonedCartSetupInfo()
                {
                    InstallDate= now,
                    SiteID = this.SiteId
                });
                this.InstallDate = now;
            }
            else
            {
                InstallDate = InstallDateInfos.First().InstallDate;
            }
            contactCarts = new List<AbandonedCartContactCart>();
        }

        public void PrepareActivitiesQuery()
        {
            
            query = ActivityInfoProvider.GetActivities();

            var activityTypes = new List<string>();
            activityTypes.Add("productaddedtoshoppingcart");
            activityTypes.Add("productremovedfromshoppingcart");
            activityTypes.Add("purchase");
            activityTypes.Add("productunitschanged");

            query = query.WhereEquals("ActivitySiteID", SiteId);

            query = query.WhereIn("ActivityType", activityTypes);

            if (activitiesNotOlderThan > 0)
            {
                var activitiesNotOlderThanDateTime = ResolveTime(activitiesNotOlderThanUnit);
         //       EventLogProvider.LogEvent("I", "AbandonedCart", "activitiesNotOlderThan", activitiesNotOlderThanDateTime.ToString() + " " + (activitiesNotOlderThanDateTime - DateTime.Now).ToString() + " Ustawienie: " + activitiesNotOlderThanUnit);
                query = query.WhereGreaterOrEquals("ActivityCreated", activitiesNotOlderThanDateTime);
            }

            if (InstallDateSetting && InstallDate!=null)
            {
                query = query.WhereGreaterOrEquals("ActivityCreated", InstallDate);
      //          EventLogProvider.LogEvent("I", "AbandonedCartTask", "InstallDate: " + InstallDate.ToString());
            }
       //     EventLogProvider.LogEvent("I", "AbandonedCartTask", "Settings: " + InstallDateSetting + " " + (InstallDate != null));

            query = query.OrderByAscending("ActivityCreated");

            
        }
        
        public void LoadSettings()
        {
            var cartIsEnabled = SettingsKeyInfoProvider.GetBoolValue("AbandonedCartEnabled", SiteId);
            if (!cartIsEnabled)
            {
                throw new Exception("Proces został wyłączony na poziomie ustawień");
            }

            EmailTemplateIsGlobal = SettingsKeyInfoProvider.GetBoolValue("AbandonedCartEmailTemplateIsGlobal", SiteId);

            emailTemplateCodeName = SettingsKeyInfoProvider.GetValue("AbandonedCartEmailTemplate", SiteId);
            if (string.IsNullOrEmpty(emailTemplateCodeName))
            {
                throw new Exception("Niepoprawnie określona nazwa kodowa szablonu emaila");
            }
            else
            {
                if (EmailTemplateIsGlobal)
                {
                    emailTemplate = CMS.EmailEngine.EmailTemplateProvider.GetEmailTemplate(emailTemplateCodeName,null);
                }
                else
                {
                    emailTemplate = CMS.EmailEngine.EmailTemplateProvider.GetEmailTemplate(emailTemplateCodeName, SiteId);
                }

                if (emailTemplate==null)
                {
                    throw new Exception("Nie istnieje szablon emaila o podanej nazwie");
                }
            }


            timeSinceLastActivity = SettingsKeyInfoProvider.GetIntValue("AbandonedCartLastActivity", SiteId);
            if (timeSinceLastActivity < 0)
            {
                throw new Exception("Niepoprawnie określony czas od ostatniej aktywności w koszyku");
            }

            timeSinceLastActivityUnit = SettingsKeyInfoProvider.GetIntValue("AbandonedCartLastActivityUnit", SiteId);
            if (timeSinceLastActivityUnit < 1 || timeSinceLastActivityUnit > 5)
            {
                throw new Exception("Niepoprawnie określona jednostka czasu dla ostatniej aktywności w koszyku");
            }


            activitiesNotOlderThan = SettingsKeyInfoProvider.GetIntValue("AbandonedCartActivitiesNotOlderThan", SiteId);
            if (activitiesNotOlderThan <= 0)
            {
                throw new Exception("Niepoprawnie określony maksymalny czas ważności aktywności");
            }

            activitiesNotOlderThanUnit = SettingsKeyInfoProvider.GetIntValue("AbandonedCartActivitiesNotOlderThanUnit", SiteId);
            if (activitiesNotOlderThanUnit < 1 || activitiesNotOlderThanUnit > 5)
            {
                throw new Exception("Niepoprawnie określona jednostka czasu dla maksymalnego czasu ważności aktywności");
            }

            allowUseParentSKUImage = SettingsKeyInfoProvider.GetBoolValue("AbandonedCartAllowUseParentSKUImage", SiteId);

            InstallDateSetting = SettingsKeyInfoProvider.GetBoolValue("AbandonedCartSafeMode", SiteId);




            var emailPriorityInt = SettingsKeyInfoProvider.GetIntValue("AbandonedCartEmailPriority", SiteId);
            if (emailPriorityInt != 0 && emailPriorityInt != 1 && emailPriorityInt != 2)
            {
                throw new Exception("Niepoprawnie określony priorytet emaila");
            }

            emailPriority = (EmailPriorityEnum)emailPriorityInt;

            pageUrl = SettingsKeyInfoProvider.GetStringValue("AbandonedCartPageUrl");



            discountEnabled = SettingsKeyInfoProvider.GetBoolValue("AbandonedCartDiscountEnabled", SiteId);
            if (discountEnabled)
            {
                couponPrefix = SettingsKeyInfoProvider.GetValue("AbandonedCartCouponPrefix", SiteId);
                if (string.IsNullOrEmpty(couponPrefix))
                {
                    throw new Exception("Brak prefixu dla kuponu");
                }

                discountName = SettingsKeyInfoProvider.GetValue("AbandonedCartDiscountName", SiteId);
                if (string.IsNullOrEmpty(discountName))
                {
                    throw new Exception("Brak nazwy zniżki, z której proces ma korzystać");
                }
                else
                {
                    
                    discount = DiscountInfoProvider.GetDiscountInfo(discountName, SiteCodeName);
                    if (discount == null)
                    {
                        throw new Exception("Niepoprawna nazwa zniżki");
                    }
                }
            }
        }
        
        public void GetContacts()
        {
            contacts = ContactInfoProvider.GetContacts().ToList();

        }
        public void GetActivities()
        {
            activities = query.ToList();

        }

        public void GetOldCarts()
        {
            var oldCarts = AbandonedCart.AbandonedCartContactCartInfoProvider.GetAbandonedCartContactCarts().WhereEquals("SiteID",SiteId).ToList();
            foreach (AbandonedCart.AbandonedCartContactCartInfo oldCart in oldCarts)
            {
                var cart = contactCarts.Find(x => x.AbandonedCartContactCartID == oldCart.AbandonedCartContactCartID);
                
                if (cart==null)
                {
                    contactCarts.Add(new AbandonedCartContactCart(oldCart));
                }
            }

        }

        public void HandleActivities()
        {
            foreach (ActivityInfo activity in activities)
            {
                var cart = contactCarts.Find(x => x.ContactID == activity.ActivityActiveContactID);
         
                    if (cart == null)
                    {
                        var newCart = new AbandonedCartContactCart()
                            {
                                ContactID = activity.ActivityActiveContactID
                            };
                        newCart.ProcessActivity(activity);
                        contactCarts.Add(newCart);
                    }
                    else
                    {
                        cart.ProcessActivity(activity);
                    }

                }
            }
        
        public void SendEmails()
        {
            
            var now = DateTime.Now;
            var dummyCart = ShoppingCartInfo.CreateShoppingCartInfo(SiteId);

            foreach (AbandonedCartContactCart cart in contactCarts)
            {
                DateTime cartTime = ResolveTime(cart, timeSinceLastActivityUnit);

           //     EventLogProvider.LogEvent("I", "AbandonedCart", "timeSinceLastActivity", cart.LastUpdate.ToString() + " " + cartTime.ToString() + " Option:" + timeSinceLastActivityUnit);
                if (cartTime <= now)
                {
                    
                    var contact = ContactInfoProvider.GetContactInfo(cart.ContactID);
                    if (contact != null)
                    {
                        if (!string.IsNullOrEmpty(contact.ContactEmail))
                        {
                            if (cart.LastEmailSent != cart.LastUpdate)
                            {
                                bool itemsPresent = false;
                                foreach (AbandonedCartProductCycle item in cart.ProductCycles)
                                {
                                    if (item.SKUUnits>0)
                                    {
                                        itemsPresent = true;
                                    }
                                }
                                if (itemsPresent)
                                {

                                    System.Data.DataTable dataTable = new System.Data.DataTable();
                                    dataTable.Columns.Add("SKUName", typeof(string));
                                    dataTable.Columns.Add("SKUPrice", typeof(double));
                                    dataTable.Columns.Add("SKUUnits", typeof(int));
                                    dataTable.Columns.Add("SKUValue", typeof(double));
                                    dataTable.Columns.Add("SKUImage", typeof(string));

                                    double totalCartValue = 0;
                                    foreach (AbandonedCartProductCycle item in cart.ProductCycles)
                                    {
                              //          EventLogProvider.LogEvent("I", "AbandonedCartTask", "Item: " + item.SKUID + " Units:" + item.SKUUnits);        // TEMP
                                        if (item.SKUUnits > 0)
                                        {
                                            var skui = SKUInfoProvider.GetSKUInfo(item.SKUID);
                                            var SKUPrice = SKUInfoProvider.GetSKUPrice(skui, dummyCart, true, true);


                                            string imagePath = skui.GetStringValue("SKUImagePath", "");

                                            if (string.IsNullOrEmpty(imagePath) && allowUseParentSKUImage)
                                            {
                                                imagePath = skui.Parent.GetStringValue("SKUImagePath", "");
                                            }
                            //                EventLogProvider.LogEvent("I", "AbandonedCartTask", "ImageRelativePath: " + skui.GetStringValue("SKUImagePath", "") + "Item: " + item.SKUID, skui.ToXML("SKUINFO", false));       // TEMP
                                            if (string.IsNullOrEmpty(pageUrl))
                                            {
                                                imagePath = URLHelper.GetAbsoluteUrl(imagePath);
                                            }
                                            else
                                            {
                                                imagePath = imagePath.Remove(0, 2);
                                                imagePath = pageUrl + imagePath;
                                            }
                                            double SKUValue = SKUPrice * item.SKUUnits;

                                            dataTable.Rows.Add(skui.SKUName, SKUPrice, item.SKUUnits, (object)SKUValue, (object)imagePath);
                        //                    EventLogProvider.LogEvent("I", "AbandonedCartTask", "ImageAbsolutePath: " + imagePath + "Item: " + item.SKUID);       // TEMP
                        //                    EventLogProvider.LogEvent("I", "AbandonedCartTask", "Item Added: " + item.SKUID + "ItemName: " + skui.SKUName + " Units:" + item.SKUUnits + "Price: " + SKUPrice + "ItemValue: " + SKUValue);        // TEMP
                                            totalCartValue += SKUValue;
                                        }
                                    }
                                    string couponCode = "";
                                    if (discountEnabled)
                                    {
                                        if (totalCartValue >= discount.DiscountItemMinOrderAmount)
                                        {
                                            if (string.IsNullOrEmpty(cart.CurrentCouponCode))
                                            {
                                                couponCode = GenerateCodes(discount.DiscountID, couponPrefix, SiteCodeName);
                                                cart.CurrentCouponCode = couponCode;
                                            }
                                            else
                                            {
                                                couponCode = cart.CurrentCouponCode;
                                            }
                                        }
                                    }




                                    var targetEmail = contact.ContactEmail;

                                    var emailMessage = new CMS.EmailEngine.EmailMessage();
                  
                                    var resolver = CMS.MacroEngine.MacroContext.CurrentResolver;


                                    string[,] replacements = new string[10, 2];
                                    string itemList = "";
                                    string emailTitle = " Masz coś w koszyku!";






                                    replacements[0, 0] = "AbandonedCartItemList";
                                    replacements[0, 1] = itemList;
                                    replacements[1, 0] = "AbandonedCartTotalCartValue";
                                    replacements[1, 1] = totalCartValue.ToString();
                                    replacements[2, 0] = "AbandonedCartEmailTitle";
                                    replacements[2, 1] = emailTitle;
                                    replacements[3, 0] = "AbandonedCartCouponCode";
                                    replacements[3, 1] = couponCode;
                                    if (discount!=null)
                                    {
                                        if (discount.IsApplicableToOrders)
                                        {
                                            var discountTypeString = "";
                                            if (discount.ItemDiscountIsFlat)
                                            {
                                                discountTypeString = "";
                                                replacements[5, 0] = "AbandonedCartDiscountType";
                                                replacements[5, 1] = "orderfixed";
                                            }
                                            else
                                            {
                                                discountTypeString = "%";
                                                replacements[5, 0] = "AbandonedCartDiscountType";
                                                replacements[5, 1] = "orderpercent";
                                            }
                                            replacements[4, 0] = "AbandonedCartDiscountInfo";
                                            replacements[4, 1] = discount.ItemDiscountValue + discountTypeString;
                                            
                                        }
                                        if (discount.IsApplicableToShipping)
                                        {
                                            replacements[5, 0] = "AbandonedCartDiscountType";
                                            replacements[5, 1] = "shipping";
                                        }
                                    }
                                    

                                    resolver.SetNamedSourceData(replacements);
                                    resolver.SetNamedSourceData("AbandonedCartItems", dataTable.Rows);

                                    emailMessage.EmailFormat = CMS.EmailEngine.EmailFormatEnum.Default;
                                    //        emailMessage.From = emailSender;
                                    emailMessage.Recipients = targetEmail;
                                    emailMessage.Priority = emailPriority;

                                    EmailSender.SendEmailWithTemplateText(SiteCodeName, emailMessage, emailTemplate, resolver, false);


                                    cart.LastEmailSent = cart.LastUpdate;



                                }
                            }
                        }


                    }
                }
                }

        }

        public void SaveChanges()
        {
            foreach (AbandonedCartContactCart cart in contactCarts)
            {
                cart.SaveCart();
            }
        }

        private DateTime ResolveTime(AbandonedCartContactCart cart, int option)
        {
            DateTime time = DateTime.Now;
                if (option==1)
                {
                    time = cart.LastUpdate.AddSeconds(timeSinceLastActivity);
                }
                if (option==2)
                {
                    time = cart.LastUpdate.AddMinutes(timeSinceLastActivity);
                }
                if (option==3)
                {
                    time = cart.LastUpdate.AddHours(timeSinceLastActivity);
                }
                if (option==4)
                {
                    time = cart.LastUpdate.AddDays(timeSinceLastActivity);
                }

            return time;
        }

        private DateTime ResolveTime(int option)
        {
            DateTime time = DateTime.Now;
                if (option==1)
                {
                    time = DateTime.Now.AddSeconds(-activitiesNotOlderThan);
                }
                if (option==2)
                {
                    time = DateTime.Now.AddMinutes(-activitiesNotOlderThan);
                }
                if (option==3)
                {
                    time = DateTime.Now.AddHours(-activitiesNotOlderThan);
                }
                if (option==4)
                {
                    time = DateTime.Now.AddDays(-activitiesNotOlderThan);
                }

            return time;
        }


        private static string GenerateCodes(int idZnizki, string prefix = "AC", string siteCodeName="")
        {
            try
            {
                // Construct cache for code uniqueness check
                var existingCodes = GetExistingCodes(siteCodeName);

                CouponCodeInfo couponCode = null;

                using (CMSActionContext context = new CMSActionContext())
                {
                    // Do not touch parent for all codes
                    context.TouchParent = false;
                    context.LogEvents = false;

                    // Create generator
                    RandomCodeGenerator generator = new RandomCodeGenerator(prefix + "######", "");
                    // Use cache for checking code uniqueness
                    generator.CodeChecker = code => !existingCodes.Contains(code);


                    // Get new code
                    string kod = generator.GenerateCode();

                    couponCode = new CouponCodeInfo
                    {
                        CouponCodeUseLimit = 1,
                        CouponCodeDiscountID = idZnizki,
                        CouponCodeCode = kod,
                        CouponCodeUseCount = 0
                    };

                    // Save coupon code
                    CouponCodeInfoProvider.SetCouponCodeInfo(couponCode);
                    // Touch parent (one for all)
                    if (couponCode != null)
                    {
                        couponCode.Generalized.TouchParent();
                    }

                    return kod;
                }




            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("Discounts", "GENERATECOUPONCODES", ex);
                return null;
            }
        }


        private static HashSet<string> GetExistingCodes(string siteCodeName)
        {
            // Prepare query for codes cache
            var existingQuery = CouponCodeInfoProvider.GetCouponCodes(siteCodeName).Column("CouponCodeCode").Distinct();



            // Create cache of this site coupon codes
            var existingCodes = new HashSet<string>();
            using (DbDataReader reader = existingQuery.ExecuteReader())
            {
                while (reader.Read())
                {
                    existingCodes.Add(reader.GetString(0));
                }
            }

            return existingCodes;
        }

    
    }
}