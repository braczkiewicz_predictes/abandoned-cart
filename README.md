# __Abandoned Cart - Wersja Multi-Step!__


## Change Log

* 2015/07/03 - bugfix: teraz proces poprawnie korzysta ze swoich lokalnych ustawień na sajcie, zamiast korzystać wyłącznie z ustawień globalnych
* 2015/07/06 - bugfix: proces teraz poprawnie korzysta z kontekstu TaskInfo zamiast SiteContext
* 2015/07/07 - bugfix: SiteCodeName jest teraz poprawnie inicjalizowany, zniżki są teraz obsługiwane poprawnie
* 2015/07/15 - przejście na wersję Multi-Step

## Setup

### Wymagania wstępne:

* Modyfikacja już istniejącego pliku CartItemUnits.ascx.cs z kontrolką obsługującą zmianę ilości produktów wewnątrz koszyka

 o następujący kod ([LINK DO ANALOGICZNEGO JUŻ ZMODYFIKOWANEGO PLIKU Z TAROSA](https://bitbucket.org/braczkiewicz_predictes/abandoned-cart/downloads/CartItemUnits.ascx.cs)) :



```
#!c#
//==== NEW CODE
                ActivityTypeInfo activityType = ActivityTypeInfoProvider.GetActivityTypeInfo("productunitschanged");
                if (activityType != null)
                {


                    ActivityInfo activity = new ActivityInfo()
                    {
                        ActivityType = activityType.ActivityTypeName,
                        ActivityTitle = "Product '" + ShoppingCartItemInfoObject.SKU.SKUName + "' shopping cart units changed",
                        ActivityCreated = DateTime.Now,
                        ActivityItemID = ShoppingCartItemInfoObject.SKUID,
                        ActivitySiteID = SiteContext.CurrentSiteID,
                        ActivityActiveContactID = OnlineMarketingContext.CurrentContact.ContactID,
                        ActivityOriginalContactID = OnlineMarketingContext.CurrentContact.ContactID,
                        ActivityValue = Convert.ToString(count)

                    };
                    ActivityInfoProvider.SetActivityInfo(activity);

                }
                else
                {
                    EventLogProvider.LogEvent("I", "CartItemUnits", "Brak typu aktywności: productunitschanged");
                }
//==== NEW CODE
```


*  Dodanie customowej aktywności o nazwie kodowej: "productunitschanged" (nie ma jej w paczce!)

### Instalacja na instancjach z zainstalowaną wcześniej wersją Single-Step __(ważne!)__

* Należy usunąć najbardziej zewnętrzną _grupę ustawień AbandonedCart_ (to jest - tak, aby usunięte zostały wszystkie klucze ustawień wraz z grupami).

### Zasadnicza instalacja [MODUŁU](https://bitbucket.org/braczkiewicz_predictes/abandoned-cart/downloads/AbandonedCart_MultiStep_v1.0.8_20150715_1548.zip)

Instalacja samego modułu jest nieskomplikowana, instalujemy jak zwykły site import.

Przy imporcie na wersję wyższą niż 8.2.19 należy __zaznaczyć import plików!__

Zaimportowany scheduled task powinien być domyślnie wyłączony.

### Dodatkowe pliki

* [Templatki emaili dla Limody](https://bitbucket.org/braczkiewicz_predictes/abandoned-cart/downloads/Limoda_AC_EmailTemplates_20150722_1148.zip)

### Dostosowanie ustawień po instalacji

Domyślne ustawienia są bezpieczne.

Aby proces mógł zadziałać spełnione muszą być dwa następujące warunki: 

*  Scheduled Task musi być włączony/odpalany ręcznie

*  Ustawienie _AbandonedCartModuleEnable_ musi mieć wartość __true__

*  Moduł musi posiadać przynajmniej włączony, poprawnie zdefiniowany krok na danej stronie